package vectores;

import java.util.Random;
import java.util.Scanner;

public class OCADAM {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		Random rnd = new Random();
		final int N = 32;
		int i, op;
		int[] vector;
		vector = new int[N];
		boolean surt = false;
		int casella1, casella2;
		int dau;
		int usuari = 0;
		casella1 = casella2 = op = 0;
		while (surt == false) {
			do {
				System.out.println();
				System.out.print("1. Inicialitzar. \n");
				System.out.print("2. Aparellar. \n");
				System.out.print("3. Veure joc. \n");
				System.out.print("4. Llen�ar. \n");
				System.out.print("0. Sortir. \n");

				try {
					op = src.nextInt();
				} catch (Exception e) {
					System.out.println("Atenci�: �nicament es permet insertar n�meros enters.");
					src.hasNextInt();
				}
			} while (op < 0 || op > 4);
			switch (op) {
			case 1:
				for (i = 0; i < N; i++)
					vector[i] = 0;
				break;
			case 2:
				System.out.print("Casella1: ");
				casella1 = src.nextInt();
				System.out.print("Casella2: ");
				casella2 = src.nextInt();
				if ((casella1>1 && casella1<32) || (casella2 > 1 && casella2<32)) {
					if (vector[casella1] == 0 && vector[casella2] == 0) {
						vector[casella1] = casella2;
						vector[casella2] = casella1;
					}	 else
						System.out.print("Error.");
				}
				else
					System.out.print("Introdueix una casella entre 2 i 31");
				break;
			case 3:
				for (i = 0; i < N; i++) {
					System.out.printf("%3d",i);
					
				}
				System.out.println();
				for (i=0; i<N; i++) {
					if (i==usuari) {
						System.out.print("  X");
					}
					else if (vector[i]==0) {
						System.out.print("   ");
					}
					else {
						System.out.printf("%3d",vector[i]);
					}
				}
				break;
			case 4:
				System.out.print("Estas a la casella: " + usuari);
				dau = rnd.nextInt(6) + 1;
				usuari=usuari+dau;
				System.out.println();
				System.out.print("Has tret un: " + dau);
				System.out.println();
				System.out.print("Despres de llen�ar el dau estas a la casella: " + usuari);
				
				if (usuari>32) {
					usuari = 32;
				}
				if (vector[usuari]!=0) {
					usuari = vector[usuari];
				}
				if (usuari==32) {
					System.out.print("Has guanyat.");
					surt=true;
				}
				break;
			case 0:
				surt = true;
				break;
			}
		}
	}

}
