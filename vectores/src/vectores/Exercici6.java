package vectores;

import java.util.Random;
import java.util.Scanner;

public class Exercici6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		Random rnd = new Random();
		int op = 0;

		do {
			System.out.print("Men� d'opcions: \n ");
			System.out.print("1. Exercici1 \n");
			System.out.print("2. Exercici2 \n");
			System.out.print("3. Exercici3 \n");
			System.out.print("4. Exercici4 \n");
			System.out.print("5. Exercici5 \n");
			System.out.print("0. Sortir");

			System.out.print("\nIntrodueix una opci�: ");
			try {
				op = src.nextInt();
			} catch (Exception e) {
				System.out.println("Atenci�: �nicament es permet insertar n�meros enters.");
				src.hasNextInt();
			}
		} while (op < 0 || op > 5);

		switch (op) {
		case 1:
			final int N1;

			int[] vector;
			int pos, suma;
			suma = 0;
			System.out.print("Indica el tama�o del vector: ");
			N1 = src.nextInt();

			vector = new int[N1];

			pos = 0;

			while (pos < N1) {
				System.out.print("Valor de la posicion " + pos + ": ");
				vector[pos] = src.nextInt();
				pos++;
			}

			pos = 0;
			while (pos < N1) {
				if (pos % 3 == 0) {
					suma = suma + vector[pos];
				}
				pos++;
			}
			System.out.println("La suma �s: " + suma);
			break;
		case 2:
			final int MAX2 = 10;
			int[] vector2;
			int i2 = 0;
			int valor2 = 0;
			boolean correcte2 = false;

			vector2 = new int[MAX2];

			for (i2 = 0; i2 < MAX2; i2++) {
				vector2[i2] = rnd.nextInt(100);

			}

			correcte2 = false;
			while (!correcte2) {
				System.out.print("Introdueix el valor a buscar dins del vector:  ");
				try {
					valor2 = src.nextInt();
					correcte2 = true;
				} catch (Exception e) {
					System.out.println("Atenci�! �nicament es permet insertar n�meros enters. ");
					src.nextLine();
				}
			}

			for (i2 = 0; i2 < MAX2; i2++)
				System.out.print(vector2[i2] + " - ");
			System.out.println();

			i2 = 0;
			correcte2 = false;
			while (!correcte2 && i2 < MAX2) {
				if (valor2 == vector2[i2])
					correcte2 = true;
				i2++;
			}
			if (correcte2)
				System.out.println("El valor " + valor2 + " SI es troba dins del vector ");
			else
				System.out.println("El valor " + valor2 + " NO es troba dins del vector ");

			break;
		case 3:
			final int MAX3 = 10;
			int[] vector3;
			int i = 0;
			int valor3 = 0;
			boolean correcte3 = false;
			int contMes = 0, contMenys = 0, contIgual = 0;

			vector3 = new int[MAX3];

			for (i = 0; i < MAX3; i++) {
				vector3[i] = rnd.nextInt(100);
			}

			while (!correcte3) {
				System.out.print("Introdueix el valor a buscar dins del vector:  ");
				try {
					valor3 = src.nextInt();
					correcte3 = true;
				} catch (Exception e) {
					System.out.println("Atenci�! �nicament es permet insertar n�meros enters. ");
					src.nextLine();
				}
			}

			for (i = 0; i < MAX3; i++)
				System.out.print(vector3[i] + " - ");
			System.out.println();

			for (i = 0; i < MAX3; i++) {
				if (valor3 < vector3[i])
					contMes++;
				else {
					if (valor3 == vector3[i]) {
						contIgual++;
					} else {
						contMenys++;
					}
				}
			}
			System.out.println("S'han trobat " + contMes + " valors majors que " + valor3);
			System.out.println("S'han trobat " + contMenys + " valors menors que " + valor3);
			System.out.println("S'han trobat " + contIgual + " valors iguals a " + valor3);
			break;
		case 4:
			final int MAX4 = 10;
			int[] vector4;
			int i4 = 0;
			int valor = 0;
			boolean correcte4 = false;
			int zero, un, dos, tres, quatre, cinc, sis, set, vuit, nou, deu;

			vector4 = new int[MAX4];

			for (i4 = 0; i4 < MAX4; i4++) {
				vector4[i4] = rnd.nextInt(10);
			}

			for (i4 = 0; i4 < MAX4; i4++)
				System.out.print(vector4[i4] + " - ");
			System.out.println();

			zero = un = dos = tres = quatre = cinc = sis = set = vuit = nou = deu = 0;
			for (i4 = 0; i4 < MAX4; i4++) {
				switch (vector4[i4]) {
				case 0:
					zero++;
					break;
				case 1:
					un++;
					break;
				case 2:
					dos++;
					break;
				case 3:
					tres++;
					break;
				case 4:
					quatre++;
					break;
				case 5:
					cinc++;
					break;
				case 6:
					sis++;
					break;
				case 7:
					set++;
					break;
				case 8:
					vuit++;
					break;
				case 9:
					nou++;
					break;
				case 10:
					deu++;
					break;
				}
			}
			System.out.println("Total de zeros: " + zero);
			System.out.println("Total de uns: " + un);
			System.out.println("Total de dos: " + dos);
			System.out.println("Total de tresos: " + tres);
			System.out.println("Total de quatres: " + quatre);
			System.out.println("Total de cincs: " + cinc);
			System.out.println("Total de sisos: " + sis);
			System.out.println("Total de sets: " + set);
			System.out.println("Total de vuits: " + vuit);
			System.out.println("Total de nous: " + nou);
			System.out.println("Total de deus: " + deu);
			break;
		case 5:
			final int MAX5 = 10;
			int[] vector5;
			vector5 = new int[MAX5];
			int num5 = 0;
			int pos5 = 0;
			System.out.print("Valor de la posicion " + pos5 + ": ");
			vector5[pos5] = src.nextInt();
			if (num5 >= 0 && num5 <= 100) {
				while (pos5 < MAX5) {
					if (vector5[pos5 - 1] > vector5[pos5]) {
						System.out.print("Valor de la posicion " + pos5 + ": ");
						vector5[pos5] = src.nextInt();
					} else {
						pos5++;
					}

				}
			} else
				System.out.println("Error. Introdueix un numero del 0 al 100.");
			break;
		case 0:

			break;
		}
	}
}
