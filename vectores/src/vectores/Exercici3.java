package vectores;

import java.util.Random;

/* Ex 3. Fer un programa per omplir un vector de
 *  10 elementos de valors enters. Despr�s demanar� a l�usuari un n� enter i el programa informar� de quants valors s�n m�s grans, quants s�n m�s petits i quants s�n iguals al n�mero introdu�t.
 */

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades


public class Exercici3 {
		
		public static void main (String [] args) {
			Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			Random rnd = new Random();
			
			final int MAX = 10;	
			int [] vector;  //int vector[];
			int i = 0;
			int valor = 0;
			boolean correcte = false;
			int contMes = 0, contMenys = 0, contIgual = 0;
			
			//Creaci� del vector amb MAX posicions
			vector = new int[MAX];
			
			//Omplim el vector de valors aleatoris
			
			for (i = 0; i < MAX; i++) {
				//vector[i] = (int) Math.floor(Math.random()*(100-0+1));  //double valorEntero = Math.floor(Math.random()*(N-M+1)+M);  // Valor entre M y N, ambos incluidos.
				vector[i] = rnd.nextInt(100);
			}
			
			//Obtenim el valor a buscar
			
			while (!correcte) {
				System.out.print("Introdueix el valor a buscar dins del vector:  ");
				try {
					valor = reader.nextInt(); 
					correcte = true; 
				} catch (Exception e){
					System.out.println("Atenci�! �nicament es permet insertar n�meros enters. ");
					reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero
				}	
			}
			
			//Mostrem el contingut del vector
			for (i = 0; i < MAX; i++) 
				System.out.print(vector[i] + " - ");
			System.out.println();
			
			//Calculem quants valors son majors, menors o iguals a valor
			for (i = 0; i < MAX; i++) {   //Cal recorre tot el vector
				if (valor < vector[i]) 
					contMes++;
				else {
					if (valor == vector[i]) {
						contIgual++;
					}
					else {
						contMenys++;
					}
				}
			}
			System.out.println("S'han trobat " + contMes + " valors majors que " + valor);
			System.out.println("S'han trobat " + contMenys + " valors menors que " + valor);
			System.out.println("S'han trobat " + contIgual + " valors iguals a " + valor);
				
		}  //Fi del main
}



