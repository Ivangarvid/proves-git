package vectores;

import java.util.Random;
import java.util.Scanner;

/*
 * Fer un programa per omplir un vector de N elements de valors enters entre 0 i 10 (inclosos). Despr�s demanar� a l�usuari un n� enter i el programa respondr� si aquest valor es troba al vector o no. N �s una constant definida pel programador. Els valors dels elements seran generats autom�ticament pel programa mitjan�ant un generador de n�meros aleatoris de java.
 */

public class Exercici2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		Random rnd = new Random();

		final int MAX = 10;
		int[] vector;
		int i = 0;
		int valor = 0;
		boolean correcte = false;

		vector = new int[MAX];

		for (i = 0; i < MAX; i++) {
			vector[i] = rnd.nextInt(100);

		}

		correcte = false;
		while (!correcte) {
			System.out.print("Introdueix el valor a buscar dins del vector:  ");
			try {
				valor = src.nextInt();
				correcte = true;
			} catch (Exception e) {
				System.out.println("Atenci�! �nicament es permet insertar n�meros enters. ");
				src.nextLine();
			}
		}

		for (i = 0; i < MAX; i++)
			System.out.print(vector[i] + " - ");
		System.out.println();

		i = 0;
		correcte = false;
		while (!correcte && i < MAX) {
			if (valor == vector[i])
				correcte = true;
			i++;
		}
		if (correcte)
			System.out.println("El valor " + valor + " SI es troba dins del vector ");
		else
			System.out.println("El valor " + valor + " NO es troba dins del vector ");

		}
	}


