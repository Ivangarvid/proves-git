package vectores;

import java.util.Random;
import java.util.Scanner;

public class Exercici4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		
		final int MAX = 10;	
		int [] vector;  //int vector[];
		int i = 0;
		int valor = 0;
		boolean correcte = false;
		int zero, un, dos, tres, quatre, cinc, sis, set, vuit, nou, deu;
		
		//Creaci� del vector amb MAX posicions
		vector = new int[MAX];
		
		//Omplim el vector de valors aleatoris
		
		for (i = 0; i < MAX; i++) {
			//vector[i] = (int) Math.floor(Math.random()*(21));  //double valorEntero = Math.floor(Math.random()*(N-M+1)+M);  // Valor entre M y N, ambos incluidos.
			vector[i] = rnd.nextInt(10);		
		}
		
		
		//Mostrem el contingut del vector
		for (i = 0; i < MAX; i++) System.out.print(vector[i] + " - ");
			System.out.println();
		
		zero = un = dos = tres = quatre = cinc = sis = set = vuit = nou = deu = 0;
		for (i = 0; i < MAX; i++) {
			switch (vector[i]) {
				case 0: zero++;
				break;
				case 1: un++;
				break;
				case 2: dos++;
				break;
				case 3: tres++;
				break;
				case 4: quatre++;
				break;
				case 5: cinc++;
				break;
				case 6: sis++;
				break;
				case 7: set++;
				break;
				case 8: vuit++;
				break;
				case 9: nou++;
				break;
				case 10: deu++;
				break;
			}
		}
		System.out.println("Total de zeros: " + zero);
		System.out.println("Total de uns: " + un);
		System.out.println("Total de dos: " + dos);
		System.out.println("Total de tresos: " + tres);
		System.out.println("Total de quatres: " + quatre);
		System.out.println("Total de cincs: " + cinc);
		System.out.println("Total de sisos: " + sis);
		System.out.println("Total de sets: " + set);
		System.out.println("Total de vuits: " + vuit);
		System.out.println("Total de nous: " + nou);
		System.out.println("Total de deus: " + deu);
		
	}  //Fi del main
}
