package vectores;

import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		
		final int N;
		
		int [] vector;
		int pos, suma;
		suma = 0;
		System.out.print("Indica el tama�o del vector: ");
		N=src.nextInt();
		
		vector = new int [N];
		
		pos=0;
		
		while (pos < N) {
			System.out.print("Valor de la posicion " + pos + ": ");
			vector[pos] = src.nextInt();
			pos++;
		}
		
		pos=0;
		while (pos < N) {
			if (pos % 3 == 0) {
				suma= suma + vector [pos];
			}
			pos++;
		}
		System.out.println("La suma �s: " + suma);
		}

}
